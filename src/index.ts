import * as backoff from 'backoff';
import * as log from 'fancy-log';
import * as mongoose from 'mongoose';

export interface ConnectOptions {
  name?: string;
  host?: string;
  port?: string;
  tries?: number;
  initialDelay?: number;
  maxDelay?: number;
}

function loadPkgJson(attempts: number = 1) {
  if (attempts > 5) {
    return 'default';
  }
  const mainPath = attempts === 1 ? './' : Array(attempts).join('../');
  try {
    return require.main ? require.main.require(mainPath + 'package.json') : '';
  } catch (e) {
    return loadPkgJson(attempts + 1);
  }
}

function connect(options: ConnectOptions): Promise<void> {
  return new Promise((resolve, reject) => {

    const {
      name = loadPkgJson(),
      host = 'database',
      port = '27017',
      tries = 32,
      initialDelay = 500,
      maxDelay = 4096000,
    } = options;

    const expBackoff = backoff.exponential({ initialDelay, maxDelay });

    expBackoff.failAfter(tries);

    expBackoff.on('backoff', (num, delay) => {
      if (num > 0) {
        log.error('Could not connect to MongoDB. Retrying in ' + (delay / 1000) + 's');
      }
    });

    expBackoff.on('ready', () => {
      const uri = `mongodb://${host}:${port}/${name}`;
      (mongoose as any).Promise = global.Promise;
      mongoose.connect(uri, { useMongoClient: true } as any)
        .then(() => {
          log.info('Connected to MongoDB');
          expBackoff.reset();
          return resolve();
        })
        .catch(() => {});
    });

    mongoose.connection.on('disconnected', () => {
      expBackoff.backoff();
    });

    expBackoff.on('fail', () => {
      log.error('Failed to connect to MongoDB');
      return reject();
    });

    expBackoff.backoff();
  });
}

export default connect;
